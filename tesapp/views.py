from django.shortcuts import render,HttpResponse, redirect
from .models import Statuss
from .forms import Formhome
from datetime import datetime
def home(request):

    
    if(request.method == "POST"):
        form = Formhome(request.POST)
        if(form.is_valid()):
            date=datetime.now()
            date=date.strftime("%d %b %Y %H:%M")
            objek = Statuss(           
                status = form.data['status'],
                tanggal=date)    
            objek.save()
            return redirect('/')
        else :
            pk = request.POST['id']
            obj = Statuss.objects.get(pk=pk)
            obj.delete()
            return redirect('/')
    form = Formhome()

    list_status = Statuss.objects.all()
    
        
    
    context = {
        'formPavo':form,
        'list_status': list_status
    }


    return render(request,'home.html', context)