from django.test import TestCase, Client
from django.urls import resolve
from .views import home
from .models import Statuss
from datetime import datetime
import time
from selenium.webdriver.chrome.options import Options
import unittest
from selenium import webdriver
from django.test import LiveServerTestCase


class unitTest(TestCase):

    def test_url_is_existed(self):
        response=Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_template_exist(self):
        response=Client().get('/')
        self.assertTemplateUsed(response,"home.html")
    
    def test_function_found(self):
        response=resolve('/')
        self.assertEqual(response.func,home)

    def test_model_statuss(self):
        date=datetime.now()
        date=date.strftime("%d %b %Y %H:%M")
        print(date)
        objek=Statuss.objects.create(status="tes", tanggal=date)
        
        jumlah=Statuss.objects.all().count()
        self.assertEqual(jumlah,1)
        
    def test_POST_form(self):
        date=datetime.now()
        date=date.strftime("%d %b %Y %H:%M")
        response=self.client.post("/", data={'status': 'tes', 'tanggal' : date})
        jumlah=Statuss.objects.all().count()
        self.assertEqual(jumlah,1)

        print(response.status_code)

        new_response=self.client.get('/')
        html_response=new_response.content.decode("utf8")
        self.assertIn("ini",html_response)

class FungtionalTes(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome('./chromedriver',chrome_options = chrome_options)
        self.browser.implicitly_wait(3)

    def tearDown(self):
        self.browser.quit()

    def test_can_navigate_site(self):
        self.browser.get('http://nimotest.herokuapp.com/')
         
            # "http://localhost:8000/"
        # self.assertRaises(AssertionError,)
        
        self.assertIn("Ungkapkan Statusmu Hari ini",self.browser.find_element_by_name("awal").text)

        self.assertIn("Riwayat Status:",self.browser.find_element_by_name("riwayat").text)
        self.browser.find_element_by_name("status").send_keys("Coba-Coba")
        time.sleep(3) # Let the user actually see something!
        self.browser.find_element_by_id("buttonclick").click()

        

        self.assertIn("Coba-Coba", self.browser.page_source)
        time.sleep(3) # Let the user actually see something!
    
        self.browser.find_element_by_name("button").click()
        time.sleep(3) # Let the user actually see something!

        self.assertNotIn("Coba-Coba", self.browser.page_source)
    
    

    if __name__ == '__main__':#
        unittest.main(warnings='ignore')